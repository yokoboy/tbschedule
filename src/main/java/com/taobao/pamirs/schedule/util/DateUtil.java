package com.taobao.pamirs.schedule.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 日期工具类
 *
 * @author yokoboy
 */
public class DateUtil {

    public static String FORMAT_YMD = "yyyy-MM-dd";
    public static String FORMAT_HMS = "yyyy-MM-dd HH:mm:ss";
    public static String FORMAT_YMD_HMS = "HH:mm:ss";

    /**
     * 将Date类型转换为字符串，默认转换类型为yyyy-MM-dd HH:mm:ss
     *
     * @param date 日期对象
     * @return 日期字符串
     */
    public static String format(Date date) {
        return format(date, FORMAT_YMD);
    }

    /**
     * 将Date类型转换为字符串
     *
     * @param date    日期类型
     * @param pattern 字符串格式，如果pattern为"",null,"null",默认转换成yyyy-MM-dd HH:mm:ss
     * @return 日期字符串
     */
    public static String format(Date date, String pattern) {
        if (date == null) {
            return "";
        }
        if (pattern == null || pattern.equals("") || pattern.equals("null")) {
            pattern = FORMAT_YMD;
        }
        return new java.text.SimpleDateFormat(pattern).format(date);
    }

    /**
     * 将字符串转换为Date类型
     *
     * @param date 字符串类型，格式必须符合yyyy-MM-dd HH:mm:ss
     * @return 日期类型
     */
    public static Date format(String date) {
        return format(date, null);
    }

    /**
     * 将字符串转换为Date类型
     *
     * @param date    字符串类型
     * @param pattern 格式yyyy-MM-dd HH:mm:ss（年-月-日 时:分:秒） 您也可以这样进行转换yyyy/MM/dd等
     * @return 日期类型
     */
    public static Date format(String date, String pattern) {
        if (pattern == null || pattern.trim().equals("") || pattern.trim().toLowerCase().equals("null")) {
            pattern = FORMAT_YMD;
        }
        if (date == null || date.trim().equals("") || date.trim().toLowerCase().equals("null")) {
            return new Date();
        }
        try {
            return new SimpleDateFormat(pattern).parse(date);
        } catch (ParseException pe) {
            throw new RuntimeException("日期字符串与匹配字符串不相符。", pe);
        }
    }

}