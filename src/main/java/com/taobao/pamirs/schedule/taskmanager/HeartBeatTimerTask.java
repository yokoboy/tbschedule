package com.taobao.pamirs.schedule.taskmanager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.TimerTask;

public class HeartBeatTimerTask extends TimerTask {

    private static final Logger LOGGER = LoggerFactory.getLogger(HeartBeatTimerTask.class);

    TBScheduleManager manager;

    public HeartBeatTimerTask(TBScheduleManager aManager) {
        manager = aManager;
    }

    @Override
    public void run() {
        try {
            Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
            manager.refreshScheduleServerInfo();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }
}
