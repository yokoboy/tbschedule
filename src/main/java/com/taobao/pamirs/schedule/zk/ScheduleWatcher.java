package com.taobao.pamirs.schedule.zk;

import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.Watcher.Event.KeeperState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ScheduleWatcher implements Watcher {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScheduleWatcher.class);

    private Map<String, Watcher> route = new ConcurrentHashMap<>();

    private ZKManager zkManager;

    public ScheduleWatcher(ZKManager zkManager) {
        this.zkManager = zkManager;
    }

    public void registerChildrenChanged(String path, Watcher watcher) throws Exception {
        zkManager.getZooKeeper().getChildren(path, true);
        route.put(path, watcher);
    }

    public void process(WatchedEvent event) {

        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("已经触发了" + event.getType() + ":" + event.getState() + "事件！" + event.getPath());
        }

        if (event.getType() == Event.EventType.NodeChildrenChanged) {
            String path = event.getPath();
            Watcher watcher = route.get(path);
            if (watcher != null) {
                try {
                    watcher.process(event);
                } finally {
                    try {
                        if (zkManager.getZooKeeper().exists(path, null) != null) {
                            zkManager.getZooKeeper().getChildren(path, true);
                        }
                    } catch (Exception e) {
                        LOGGER.error(path + ":" + e.getMessage(), e);
                    }
                }
            } else {
                LOGGER.info("已经触发了" + event.getType() + ":" + event.getState() + "事件！" + event.getPath());
            }
        } else if (event.getState() == KeeperState.AuthFailed) {
            LOGGER.info("tb_hj_schedule zk status =KeeperState.AuthFailed！");
        } else if (event.getState() == KeeperState.ConnectedReadOnly) {
            LOGGER.info("tb_hj_schedule zk status =KeeperState.ConnectedReadOnly！");
        } else if (event.getState() == KeeperState.Disconnected) {
            LOGGER.info("tb_hj_schedule zk status =KeeperState.Disconnected！");
            try {
                zkManager.reConnection();
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
            }
        } else if (event.getState() == KeeperState.NoSyncConnected) {
            LOGGER.info("tb_hj_schedule zk status =KeeperState.NoSyncConnected！等待重新建立ZK连接.. ");
            try {
                zkManager.reConnection();
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
            }
        } else if (event.getState() == KeeperState.SaslAuthenticated) {
            LOGGER.info("tb_hj_schedule zk status =KeeperState.SaslAuthenticated！");
        } else if (event.getState() == KeeperState.Unknown) {
            LOGGER.info("tb_hj_schedule zk status =KeeperState.Unknown！");
        } else if (event.getState() == KeeperState.SyncConnected) {
            LOGGER.info("收到ZK连接成功事件！");
        } else if (event.getState() == KeeperState.Expired) {
            LOGGER.error("会话超时，等待重新建立ZK连接...");
            try {
                zkManager.reConnection();
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
    }
}
