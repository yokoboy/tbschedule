<%@page import="com.taobao.pamirs.schedule.ConsoleManager" %>
<%@page import="com.taobao.pamirs.schedule.model.ScheduleStrategyVO" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<html>
<head>
    <title>
        创建调度任务
    </title>
</head>
<body bgcolor="#ffffff">

<%
    String action = request.getParameter("action");
    String result = "";
    boolean isRefreshParent = false;
    ScheduleStrategyVO scheduleStrategyVO = new ScheduleStrategyVO();
    scheduleStrategyVO.setStrategyName(request.getParameter("strategyName"));
    try {
        if (action.equalsIgnoreCase("createScheduleStrategy") || action.equalsIgnoreCase("editScheduleStrategy")) {
            scheduleStrategyVO.setKind(ScheduleStrategyVO.Kind.valueOf(request.getParameter("kind")));
            scheduleStrategyVO.setTaskName(request.getParameter("taskName"));
            scheduleStrategyVO.setTaskParameter(request.getParameter("taskParameter"));

            scheduleStrategyVO.setNumOfSingleServer(request.getParameter("numOfSingleServer") == null ? 0 : Integer.parseInt(request.getParameter("numOfSingleServer")));
            scheduleStrategyVO.setAssignNum(request.getParameter("assignNum") == null ? 0 : Integer.parseInt(request.getParameter("assignNum")));
            if (request.getParameter("ips") == null) {
                scheduleStrategyVO.setIPList(new String[0]);
            } else {
                scheduleStrategyVO.setIPList(request.getParameter("ips").split(","));
            }
            if (action.equalsIgnoreCase("createScheduleStrategy")) {
                ConsoleManager.getScheduleStrategyManager().createScheduleStrategy(scheduleStrategyVO);
                isRefreshParent = true;
            } else if (action.equalsIgnoreCase("editScheduleStrategy")) {
                ConsoleManager.getScheduleStrategyManager().updateScheduleStrategy(scheduleStrategyVO);
                isRefreshParent = true;
            }
        } else if (action.equalsIgnoreCase("deleteScheduleStrategy")) {
            ConsoleManager.getScheduleStrategyManager().deleteMachineStrategy(scheduleStrategyVO.getStrategyName());
            isRefreshParent = true;
        } else if (action.equalsIgnoreCase("pauseTaskType")) {
            ConsoleManager.getScheduleStrategyManager().pause(scheduleStrategyVO.getStrategyName());
            isRefreshParent = true;
        } else if (action.equalsIgnoreCase("resumeTaskType")) {
            ConsoleManager.getScheduleStrategyManager().resume(scheduleStrategyVO.getStrategyName());
            isRefreshParent = true;
        } else {
            throw new Exception("不支持的操作：" + action);
        }
    } catch (Throwable e) {
        e.printStackTrace();
        result = "ERROR:" + e.getMessage();
        isRefreshParent = false;
    }
%>
<%=result%>
</body>
</html>
<%
    if (isRefreshParent == true) {
%>
<script>
    parent.location.reload();
</script>
<%
    }
%>
