package com.taobao.pamirs.schedule.test;


import com.taobao.pamirs.schedule.strategy.TBScheduleManagerFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * 调度测试
 *
 * @author xuannan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/schedule.xml")
public class StartDemoSchedule {

    @Resource
    TBScheduleManagerFactory scheduleManagerFactory;

    @Test
    public void testRunData() throws Exception {
        Thread.sleep(100000000000000L);
    }
}
